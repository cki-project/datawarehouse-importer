#!/usr/bin/env python3
"""Import data into datawarehouse storage."""
# pylint: disable=invalid-name
import argparse
import distutils.util
import json
import os
import threading
import typing

import eventlet
import flask
from flask_socketio import SocketIO
from flask_wtf.csrf import CSRFProtect
from hotqueue import HotQueue
from redis import Redis

eventlet.monkey_patch()

app = flask.Flask(__name__)
app.config['SECRET_KEY'] = os.getenv('SECRET_KEY')
socketio = SocketIO(app)
csrf = CSRFProtect(app)

redis = Redis()
gitlab_queue = HotQueue('gitlab_queue', serializer=json)
task_queue = HotQueue('task_queue', serializer=json)


@socketio.on('connect')
def connected():
    """Update the web interface with everything available."""
    for task_id in redis.zrange('task_cache_index', 0, -1):
        task_event = redis.hget('task_cache', task_id)
        if task_event:
            socketio.emit('task', json.loads(task_event))
    _queue_status()


@app.route('/')
def stream():
    """Load the web interface."""
    return flask.render_template('importer.html', **locals())


@app.route('/task/<task_id>')
def pipeline_details(task_id):
    """Return a page with the task information."""
    event = json.loads(redis.hget('task_cache', task_id))
    return flask.Response(json.dumps(event, indent=2), mimetype='application/json')


@app.route('/webhook', methods=['POST'])
@csrf.exempt
def webhook():
    """Gitlab webhook."""
    if flask.request.method != 'POST':
        flask.abort(400)
    if flask.request.headers['X-Gitlab-Token'] != args.webhook_secret_token:
        flask.abort(401)
    data = flask.request.json
    if flask.request.headers['X-Gitlab-Event'] == 'Pipeline Hook':
        project_id = data['project']['id']
        pipeline_id = data['object_attributes']['id']
        gitlab_queue_put('process_pipeline', project_id, pipeline_id)
    elif flask.request.headers['X-Gitlab-Event'] == 'Job Hook':
        project_id = data['project_id']
        job_id = data['build_id']
        gitlab_queue_put('process_job', project_id, job_id)
    return '', 200


@app.route('/refresh/pipeline', methods=['POST'])
def refresh_pipeline():
    """Refresh data for pipelines."""
    project_id = int(flask.request.values['project_id'])
    if 'pipeline_id' in flask.request.values:
        pipeline_id = int(flask.request.values['pipeline_id'])
        gitlab_queue_put('process_pipeline', project_id, pipeline_id)
        gitlab_queue_put('process_jobs', project_id, pipeline_id)
    else:
        count = int(flask.request.values['count'])
        gitlab_queue_put('process_pipelines', project_id, count)
    return '', 200


@app.route('/refresh/job', methods=['POST'])
def refresh_job():
    """Refresh data for jobs."""
    project_id = int(flask.request.values['project_id'])
    job_id = int(flask.request.values['job_id'])
    gitlab_queue_put('process_job', project_id, job_id)
    return '', 200


@app.route('/queue/status', methods=['POST'])
def queue_status():
    """Enable or disable the GitLab queue."""
    set_gitlab_queue_disabled(distutils.util.strtobool(flask.request.values['disabled']))
    _queue_status()
    return '', 200


def colon_set(string: str) -> typing.Set[str]:
    """Convert a colon-separated string into a set."""
    return set(string.split(':'))


parser = argparse.ArgumentParser(description='Import data into datawarehouse storage.')
parser.add_argument('--webhook-secret-token', default=os.getenv('WEBHOOK_SECRET_TOKEN'),
                    help='Gitlab webhook secret token')
args = parser.parse_args()


def _queue_status():
    socketio.emit('queue', {
        'size': len(gitlab_queue),
        'disabled': get_gitlab_queue_disabled(),
    })


def set_gitlab_queue_disabled(value: bool):
    """Set whether the queue processing is disabled."""
    redis.set('gitlab_queue_disabled', 'true' if value else '')


def get_gitlab_queue_disabled() -> bool:
    """Return whether the queue processing is disabled."""
    return bool(redis.get('gitlab_queue_disabled'))


def gitlab_queue_put(func, *aargs, **kwargs):
    """Enqueue an arbitrary function to serialize GitLab access."""
    gitlab_queue.put((func, aargs, kwargs))
    _queue_status()


@task_queue.worker
def redis_worker(task_id=None):
    """React to redis task events."""
    task_event = json.loads(redis.hget('task_cache', task_id))
    socketio.emit('task', task_event)
    _queue_status()


threading.Thread(target=redis_worker, daemon=True).start()

socketio.run(app, host='0.0.0.0')
