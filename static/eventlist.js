import {
  Component,
  html
} from 'https://unpkg.com/htm/preact/standalone.module.js'

import { Pipeline } from './pipeline.js'

class MruPipelines {
  pipelines = {}
  keys = []

  constructor (maxPipelines) {
    this.maxPipelines = maxPipelines
  }

  set (key, pipeline) {
    this.pipelines[key] = pipeline
    this.keys = this.keys.filter(v => v !== key)
    this.keys.unshift(key)
    for (const v of this.keys.splice(this.maxPipelines)) {
      delete this.pipelines[v]
    }
  }

  use (key) {
    this.set(key, this.pipelines[key])
  }
}

export class EventList extends Component {
  state = { mru: new MruPipelines(10) }
  constructor (props) {
    super(props)
    props.socket.on('task', this.onTaskEvent)
  }

  render () {
    return html`
      <dl class="eventlist">
        ${this.state.mru.keys.map(
          key => html`
            <${Pipeline} key=${key} ...${this.state.mru.pipelines[key]} />
          `
        )}
      <//>
    `
  }

  onTaskEvent = task => {
    this.setState(state => {
      const mru = state.mru

      if (task.pipeline_id in mru.pipelines) {
        mru.use(task.pipeline_id)
      } else {
        mru.set(task.pipeline_id, {
          pipeline:
            'job_id' in task
              ? {
                project_id: task.project_id,
                pipeline_id: task.pipeline_id,
                created_at: task.pipeline_created_at
              }
              : task,
          jobs: {}
        })
      }

      if ('job_id' in task) {
        mru.pipelines[task.pipeline_id].jobs[task.job_id] = task
      } else {
        mru.pipelines[task.pipeline_id].pipeline = task
      }
      return { mru }
    })
  }
}
