import {
  Component,
  html
} from 'https://unpkg.com/htm/preact/standalone.module.js'

export class GitLabRefresh extends Component {
  fieldLabel () {
    return 'count' in this.props
      ? 'Count'
      : this.props.type === 'pipeline'
        ? 'Pipeline'
        : 'Job'
  }

  fieldName () {
    return 'count' in this.props
      ? 'count'
      : this.props.type === 'pipeline'
        ? 'pipeline_id'
        : 'job_id'
  }

  renderFull () {
    return html`
      <form action="javascript:void(0)" onSubmit=${this.submit}>
        <label>
          ${this.fieldLabel()}
          <input
            type="text"
            name=${this.fieldName()}
            value=${this.props.value || ''}
          />
        <//>
        <label>
          Project
          <input
            type="text"
            name="project_id"
            defaultValue=${this.props.projectId}
          />
        <//>
        <input type="hidden" name="csrf_token" value=${window.csrf_token} />
        <input type="submit" value="Refresh" />
      <//>
    `
  }

  renderIconOnly () {
    return html`
      <form
        action="javascript:void(0)"
        onSubmit=${this.submit}
        style="display: inline"
      >
        <input
          type="hidden"
          name=${this.fieldName()}
          value=${this.props.value}
        />
        <input type="hidden" name="project_id" value=${this.props.projectId} />
        <input type="hidden" name="csrf_token" value=${window.csrf_token} />
        <button type="submit" class="btn btn-link p-0" title="Refresh">
          <i
            class="fas fa-sync-alt ${this.props.inProgress ? 'fa-spin' : ''}"
          />
        <//>
      <//>
    `
  }

  render () {
    return this.props.iconOnly ? this.renderIconOnly() : this.renderFull()
  }

  submit = e => {
    e.preventDefault()
    const form = new FormData(e.currentTarget)
    fetch(`/refresh/${this.props.type}`, { method: 'POST', body: form })
  }
}
