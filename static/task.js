import { html } from 'https://unpkg.com/htm/preact/standalone.module.js'

import { GitLabRefresh } from './gitlabrefresh.js'

function cssClassForTaskStatus (status) {
  switch (status) {
    case 'success':
    case 'manual':
      return 'badge badge-light'
    case 'failed':
    case 'canceled':
      return 'badge badge-danger'
    case 'running':
      return 'badge badge-primary'
    case 'skipped':
      return 'badge badge-warning'
    default:
      return 'badge badge-secondary'
  }
}

const TaskError = ({ taskUrl, error = null }) => {
  return error
    ? html`
        <a title=${error} href=${taskUrl}>
          <i class="text-danger fas fa-exclamation-triangle" />
        <//>
      `
    : null
}

const TaskStatus = ({ webUrl, status = 'unknown' }) => {
  return html`
    <${webUrl ? 'a' : 'span'}
      class=${cssClassForTaskStatus(status)}
      href=${webUrl}
    >
      ${status}
    <//>
  `
}

function cssClassForSyncStatus (inSync) {
  return inSync ? 'badge badge-light' : 'badge badge-danger'
}

function firstUpper (text) {
  return text.charAt(0).toUpperCase()
}

const HumanSize = ({ size = 0 }) => {
  let humanSize = ''
  const units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
  for (const unit of units) {
    if (size < 1) break
    humanSize = `(${Math.round(size)} ${unit})`
    size /= 1000
  }
  return html`
    <span>${humanSize}<//>
  `
}

const SyncStatus = ({
  storageUrl,
  storageStatus = 'unknown',
  expectedStorageStatus = 'unknown'
}) => {
  return html`
    <${storageUrl ? 'a' : 'span'}
      href=${storageUrl}
      class=${cssClassForSyncStatus(storageStatus === expectedStorageStatus)}
      title="storage ${storageStatus}, expected ${expectedStorageStatus}"
    >
      ${firstUpper(storageStatus)}${firstUpper(expectedStorageStatus)}
    <//>
  `
}

export const Task = ({ task, name, refreshType, refreshValue }) => {
  const taskUrl = `task/${task.task_id}`
  return html`
    <li>
      <a href=${taskUrl}>${name}<//>
      ${' '}
      <${HumanSize} size=${task.storage_size} />
      ${' '}
      <${TaskStatus} webUrl=${task.web_url} status=${task.status} />
      ${' '}
      <${SyncStatus}
        storageUrl=${task.storage_url}
        storageStatus=${task.storage_status}
        expectedStorageStatus=${task.expected_storage_status}
      />
      ${' '}
      <${TaskError} taskUrl=${taskUrl} error=${task.error} />
      ${' '}
      <${GitLabRefresh}
        projectId=${task.project_id}
        type=${refreshType}
        value=${refreshValue}
        inProgress=${task.in_progress}
        iconOnly
      />
    <//>
  `
}
