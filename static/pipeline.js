import { html } from 'https://unpkg.com/htm/preact/standalone.module.js'

import { Task } from './task.js'

const TaskGroup = ({ name, tasks }) => {
  return html`
    <li>
      <b>${name}: </b>
      <ul class="taskgroup">
        ${tasks.map(
          task =>
            html`
              <${Task}
                key=${task.task_id}
                task=${task}
                name=${task.name}
                error=${task.error}
                refreshType="job"
                refreshValue=${task.job_id}
              />
            `
        )}
      <//>
    <//>
  `
}

function groupTasks (tasks) {
  return tasks.reduce((taskGroups, task) => {
    const taskGroup = taskGroups[task.task_type] || []
    taskGroup.push(task)
    taskGroups[task.task_type] = taskGroup
    return taskGroups
  }, {})
}

export const Pipeline = ({ pipeline, jobs }) => {
  return html`
    <div>
      <dt>
        <${Task}
          key=${pipeline.task_id}
          task=${pipeline}
          name="P${pipeline.pipeline_id}"
          refreshType="pipeline"
          refreshValue=${pipeline.pipeline_id}
        />
      <//>
      <dd>
        <ul>
          ${Object.entries(groupTasks(Object.values(jobs))).map(
            ([name, tasks]) => html`
              <${TaskGroup}
                key="${pipeline.pipeline_id}-${name}"
                name=${name}
                tasks=${tasks}
              />
            `
          )}
        <//>
      <//>
    <//>
  `
}
