import { html, render } from 'https://unpkg.com/htm/preact/standalone.module.js'
import { EventList } from './eventlist.js'
import { GitLabRefresh } from './gitlabrefresh.js'
import { QueueStatus } from './queuestatus.js'

const socket = io()

render(
  html`
    <div>
      <${QueueStatus} socket=${socket} />
      <${GitLabRefresh} projectId="2" type="pipeline" count value="1" />
      <${GitLabRefresh} projectId="2" type="pipeline" />
      <${GitLabRefresh} projectId="2" type="job" />
      <${EventList} socket=${socket} />
    <//>
  `,
  document.body
)
