import {
  Component,
  html
} from 'https://unpkg.com/htm/preact/standalone.module.js'

export class QueueStatus extends Component {
  state = { size: 0, disabled: false }
  constructor (props) {
    super(props)
    props.socket.on('queue', data => this.updateQueue(data))
  }

  statusString () {
    return this.state.disabled ? 'disabled' : 'enabled'
  }

  render () {
    return html`
      <div class="queuestatus">
        <span>Queue size: ${this.state.size}<//>
        ${' '}
        <span class="custom-switch">
          <input
            type="checkbox"
            class="custom-control-input"
            id="enable-queue"
            checked=${!this.state.disabled}
            onclick=${this.onClickEvent}
          />
          <label class="custom-control-label" for="enable-queue">
            ${this.statusString()}
          <//>
        <//>
      <//>
    `
  }

  updateQueue (data) {
    this.setState(() => data)
  }

  onClickEvent = e => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('csrf_token', window.csrf_token)
    formData.append('disabled', e.currentTarget.checked ? 'false' : 'true')
    fetch('/queue/status', { method: 'POST', body: formData })
  }
}
