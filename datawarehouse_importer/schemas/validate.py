"""Schema validation."""
import json


def dump_json(data, schema=None):
    """Dump JSON to a string, and validate it against the provided schema."""
    return json.dumps(validate(data, schema), sort_keys=False, indent=2)


def load_json(strdata, schema=None):
    """Load JSON from a string, and validate it against the provided schema."""
    return validate(json.loads(strdata), schema)


def validate(data, schema=None):  # pylint: disable=unused-argument
    """Validate a data structure against the provided schema."""
    return data
