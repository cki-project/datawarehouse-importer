"""Storage schema support."""
from .validate import dump_json, load_json, validate
