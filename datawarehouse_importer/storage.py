"""Datawarehouse storage."""
import functools
import io
import json
from urllib import parse

from minio import Minio
from minio.error import NoSuchBucket

from . import schemas


class Storage:
    """Datawarehouse storage abstraction."""

    PROMISE_NAME = '00-promise.json'
    ARTIFACTS_NAME = '01-artifacts/'
    RESULTS_NAME = '02-results.json'

    def __init__(self, server, access_key, secret_key):
        """Connect to a storage server."""
        if '://' in server:
            server = parse.urlsplit(server).netloc
        self.client = Minio(server, access_key=access_key, secret_key=secret_key, secure=False)

    def _read_only_policy(self, bucket: str, prefix: str) -> None:
        self.client.set_bucket_policy(bucket, json.dumps({
            "Version": "2012-10-17",
            "Statement": [{
                "Action": ["s3:GetBucketLocation"],
                "Effect": "Allow",
                "Principal": {"AWS": ["*"]},
                "Resource": [f"arn:aws:s3:::{bucket}"]
            }, {
                "Action": ["s3:ListBucket"],
                "Effect": "Allow",
                "Principal": {"AWS": ["*"]},
                "Resource": [f"arn:aws:s3:::{bucket}"],
                "Condition": {"StringEquals": {"s3:prefix": [prefix]}}
            }, {
                "Action": ["s3:GetObject"],
                "Effect": "Allow",
                "Principal": {"AWS": ["*"]},
                "Resource": [f"arn:aws:s3:::{bucket}/{prefix}*"]
            }]}))

    @staticmethod
    def storage_uri(bucket_uri, task_id):
        """Return a storage URI from a bucket URI and a task id."""
        (bucket, prefix) = Storage._split_storage_uri(bucket_uri, 'bucket_uri')
        task_prefix = f'{prefix}{task_id}/'
        return Storage._build_storage_uri(bucket, task_prefix)

    @staticmethod
    def _build_storage_uri(bucket, task_prefix, object_name=None):
        if object_name is None:
            object_name = Storage.RESULTS_NAME
        return parse.urlunsplit(('s3', bucket, '/' + task_prefix + object_name, '', ''))

    @staticmethod
    def _split_storage_uri(storage_uri, uri_type: str):
        parsed = parse.urlsplit(storage_uri)
        path = parsed.path[1:]
        if uri_type == 'bucket_uri':
            if path[-1] != '/':
                raise Exception('Bucket URI must end with /')
        elif uri_type == 'storage_uri':
            if not path.endswith(f'/{Storage.RESULTS_NAME}'):
                raise Exception(f'Storage URI must end with {Storage.RESULTS_NAME}')
            path = path[:-len(Storage.RESULTS_NAME)]
        else:
            raise Exception('Unknown URI type')
        return (parsed.netloc, path)

    def get_tasks(self, bucket_uri, task_id):
        """Return an iterator for all tasks since a specific task id."""
        (bucket, prefix) = self._split_storage_uri(bucket_uri, 'bucket_uri')
        task_prefix = f'{prefix}{task_id}'
        return ({
            'type': 'new-task',
            'storage_uri': self._build_storage_uri(bucket, o.object_name),
            'task_id': o.object_name[len(prefix):-1],
        } for o in self._ls(bucket, start_after=task_prefix, prefix=prefix))

    def _put_json(self, bucket, object_name, data):
        binary_data = schemas.dump_json(data).encode('utf-8')
        self.client.put_object(bucket, object_name,
                               io.BytesIO(binary_data), len(binary_data),
                               content_type='application/json; charset=utf-8')

    def create_promise(self, storage_uri, dependencies, data):
        """Create a promise for a new task."""
        (bucket, task_prefix) = self._split_storage_uri(storage_uri, 'storage_uri')
        object_name = f'{task_prefix}{self.PROMISE_NAME}'
        promise = {
            'meta': {'version': 1, 'file_type': 'promise'},
            'dependencies': dependencies,
            **data
        }
        self._put_json(bucket, object_name, promise)
        return self._build_storage_uri(bucket, task_prefix)

    def get_parsed_promise(self, storage_uri):
        """Get the parsed promise for an existing task."""
        (bucket, task_prefix) = self._split_storage_uri(storage_uri, 'storage_uri')
        return schemas.load_json(self.client.get_object(bucket, f'{task_prefix}/{self.PROMISE_NAME}'))

    def get_parsed_results(self, storage_uri):
        """Get the parsed results for an existing task."""
        (bucket, task_prefix) = self._split_storage_uri(storage_uri, 'storage_uri')
        return schemas.load_json(self.client.get_object(bucket, f'{task_prefix}/{self.RESULTS_NAME}'))

    def ensure_bucket(self, bucket_uri):
        """Ensure that the bucket referenced in the storage URI exists."""
        (bucket, prefix) = self._split_storage_uri(bucket_uri, 'bucket_uri')
        if not self.client.bucket_exists(bucket):
            self.client.make_bucket(bucket)
            self._read_only_policy(bucket, prefix)

    def task_status(self, storage_uri) -> str:
        """Return the task status: missing, promise or results."""
        (bucket, task_prefix) = self._split_storage_uri(storage_uri, 'storage_uri')
        objects = list(self._ls(bucket, prefix=task_prefix, recursive=True))
        print(objects)
        size = functools.reduce(lambda total_size, o: total_size + o.size, objects, 0)
        if not objects:
            return ('missing', size)
        if objects[-1].object_name.endswith(f'/{self.RESULTS_NAME}'):
            return ('results', size)
        if objects[0].object_name.endswith(f'/{self.PROMISE_NAME}'):
            return ('promise', size)
        raise Exception(f'Inconsistent storage status for {storage_uri}')

    def ls_task(self, storage_uri):
        """Return a directory listing for an existing task."""
        (bucket, task_prefix) = self._split_storage_uri(storage_uri, 'storage_uri')
        return (o.object_name[len(task_prefix):] for o in self._ls
                (bucket, prefix=task_prefix, recursive=True))

    def delete_task(self, storage_uri):
        """Delete an existing task with all objects."""
        (bucket, task_prefix) = self._split_storage_uri(storage_uri, 'storage_uri')
        for obj in self._ls(bucket, prefix=task_prefix, recursive=True):
            self.client.remove_object(bucket, obj.object_name)

    def create_results(self, storage_uri, file_type, data):
        """Create the results for an existing task."""
        (bucket, task_prefix) = self._split_storage_uri(storage_uri, 'storage_uri')
        object_name = f'{task_prefix}{self.RESULTS_NAME}'
        results = {
            'meta': {'version': 1, 'file_type': file_type},
            **data
        }
        self._put_json(bucket, object_name, results)

    def post_results(self, storage_uri, results):
        """Post the results for an existing task."""
        (bucket, task_prefix) = self._split_storage_uri(storage_uri, 'storage_uri')
        object_name = f'{task_prefix}{self.RESULTS_NAME}'
        self._put_json(bucket, object_name, schemas.load_json(results.read()))

    def post_artifact(self, storage_uri, object_name, artifact, artifact_length):
        """Post an artifact for an existing task."""
        (bucket, task_prefix) = self._split_storage_uri(storage_uri, 'storage_uri')
        object_name = f'{task_prefix}{self.ARTIFACTS_NAME}{object_name}'
        self.client.put_object(bucket, object_name, artifact, artifact_length)

    def get_artifacts(self, storage_uri):
        """Get all artifacts for an existing task."""
        (bucket, task_prefix) = self._split_storage_uri(storage_uri, 'storage_uri')
        return {o[len(self.ARTIFACTS_NAME):]: self.client.get_object(bucket, f'{task_prefix}{o}')
                for o in self.ls_task(storage_uri)
                if o.startswith(self.ARTIFACTS_NAME)}

    def get_object_watcher(self, storage_uri):
        """Return an iterator for old and new objects within a task."""
        (bucket, task_prefix) = self._split_storage_uri(storage_uri, 'storage_uri')
        task_id = task_prefix.rsplit('/', 2)[1]
        seen = set()
        for object_name in self.ls_task(storage_uri):
            seen.add(object_name)
            yield {'type': 'object',
                   'storage_uri': storage_uri,
                   'task_id': task_id,
                   'full_object_name': self._build_storage_uri(bucket, task_prefix, object_name),
                   'object_name': object_name}
        if self.RESULTS_NAME in seen:
            return
        for event in self.client.listen_bucket_notification(
                bucket, task_prefix, '', ['s3:ObjectCreated:Put']):
            for record in event['Records']:
                encoded_object_name = record['s3']['object']['key']
                object_name = parse.unquote_plus(encoded_object_name)[len(task_prefix):]
                if object_name not in seen:
                    seen.add(object_name)
                    yield {'type': 'object',
                           'storage_uri': storage_uri,
                           'task_id': task_id,
                           'full_object_name': self._build_storage_uri(bucket, task_prefix, object_name),
                           'object_name': object_name}
                if object_name == self.RESULTS_NAME:
                    return

    def get_task_watcher(self, bucket_uri):
        """Return an iterator for new tasks."""
        (bucket, prefix) = self._split_storage_uri(bucket_uri, 'bucket_uri')
        seen = set()
        for event in self.client.listen_bucket_notification(
                bucket, prefix, self.PROMISE_NAME, ['s3:ObjectCreated:Put']):
            for record in event['Records']:
                encoded_object_name = record['s3']['object']['key']
                task_prefix = parse.unquote_plus(encoded_object_name)[:-len(self.PROMISE_NAME)]
                if task_prefix not in seen:
                    seen.add(task_prefix)
                    task_id = task_prefix.rsplit('/', 2)[1]
                    yield {'type': 'task',
                           'storage_uri': self._build_storage_uri(bucket, task_prefix),
                           'task_id': task_id}

    def _ls(self, *args, **kwargs):
        """List objects, return empty list if there is no such bucket."""
        try:
            for obj in self.client.list_objects_v2(*args, **kwargs):
                yield obj
        except NoSuchBucket:
            pass
