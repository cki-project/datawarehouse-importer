"""GitLab task representation."""
# pylint: disable=no-self-use
# what to do with retriggered jobs?
# when a pipeline is created, all necessary jobs are created as well ('created')
# so going from the job names, it is possible to determine which jobs should
#   have run; on retrigger, job names will be duplicated
# all jobs created (but manual) should be executed, even skipped ones, otherwise something went wrong
# when a pipeline is canceled (because of a retrigger?), the non-executed jobs also become canceled
# when a job fails, the non-executed jobs become skipped
# if a pipeline fails (or succeeds), skipped jobs can still be retried, but
#   then they are new jobs; they have the same name, but are otherwise not
#   marked as being the same of the old one
# a manual pipeline job might be in created before it switches to manual
import datetime
import functools
import io
import zipfile

import dateutil.parser


class GitLabTask:
    """Represents a GitLab task."""

    def __init__(self, status_mappings, gitlab_project):
        """Create a task."""
        self._gitlab_project = gitlab_project
        self._fields = {
            'project_id': self._gitlab_project.id,
        }
        self._status_mappings = status_mappings

    @staticmethod
    def _task_id(created_at, *args):
        """Return the task id."""
        created_at = dateutil.parser.isoparse(created_at)
        date_utc = created_at.astimezone(datetime.timezone.utc).date().isoformat()
        fields = '-'.join(str(a) for a in args)
        return f'{date_utc}-{fields}'

    @staticmethod
    @functools.lru_cache(maxsize=128)
    def _get_stale_pipline(gitlab_project, pipeline_id: int):
        """Return a pipeline with possibly stale mutable data."""
        return gitlab_project.pipelines.get(pipeline_id)

    @staticmethod
    @functools.lru_cache(maxsize=128)
    def _get_stale_jobs(gitlab_project, pipeline_id: int):
        """Return jobs of a pipeline with possibly stale mutable data."""
        g_pipeline = GitLabTask._get_stale_pipline(gitlab_project, pipeline_id)
        return g_pipeline.jobs.list(all=True)

    @staticmethod
    @functools.lru_cache(maxsize=128)
    def _get_variables(gitlab_project, pipeline_id: int):
        """Return the variables of a pipeline."""
        g_pipeline = GitLabTask._get_stale_pipline(gitlab_project, pipeline_id)
        return g_pipeline.variables.list(all=True)

    @staticmethod
    @functools.lru_cache(maxsize=128)
    def _get_jobs(gitlab_project, pipeline_id):
        """Return the stages of a pipeline."""
        jobs = {}
        for g_job in GitLabTask._get_stale_pipline(gitlab_project, pipeline_id).jobs.list(as_list=False):
            jobs.setdefault(g_job.stage, []).append({
                'task_type': g_job.stage,
                'task_id': GitLabTask._task_id(g_job.created_at, g_job.pipeline['id'], g_job.id, g_job.stage),
            })
        return jobs

    def expected_storage_status(self):
        """Return the expected storage status."""
        status = self._fields['status']
        try:
            return next(k for k, v in self._status_mappings.items() if status in v)
        except StopIteration:
            raise Exception(f'Unknown status {status}')

    def task_type(self):
        """Return the task type."""
        return self._fields['task_type']

    def pipeline_id(self):
        """Return the pipeline id this task belongs to."""
        return self._fields['pipeline_id']

    def data(self):
        """Return the fields."""
        return self._fields


class GitLabPipeline(GitLabTask):
    """Represents a GitLab pipeline."""

    def __init__(self, gitlab_project, pipeline_id):
        """Create a pipeline."""
        super().__init__({
            'missing': ('skipped',),
            'promise': ('pending', 'running'),
            'results': ('success', 'failed', 'canceled')
        }, gitlab_project)
        self._fields['pipeline_id'] = pipeline_id

    def retrieve_fields(self):
        """Replace general information with updated info from GitLab."""
        g_pipeline = self._gitlab_project.pipelines.get(self._fields['pipeline_id'])
        g_commit = self._gitlab_project.commits.get(g_pipeline.sha)
        self._fields.update({
            'pipeline_created_at': g_pipeline.created_at,
            'git_sha': g_pipeline.sha,
            'git_ref': g_pipeline.ref,
            'git_message': g_commit.message,
            'status': g_pipeline.status,
            'web_url': g_pipeline.web_url,
            'created_at': g_pipeline.created_at,
            'started_at': g_pipeline.started_at,
            'finished_at': g_pipeline.finished_at,
            'duration': g_pipeline.duration,
            'variables': {v.key: v.value for v in self._get_variables(self._gitlab_project, g_pipeline.id)},
            'task_type': 'pipeline',
            'artifacts': [],
        })

    def task_id(self):
        """Return the task id."""
        return self._task_id(self._fields['created_at'],
                             self._fields['pipeline_id'],
                             self._fields['task_type'])

    def promise_data(self):
        """Return the immutable fields for the promise."""
        return {
            'data': {
                'task_type': self._fields['task_type'],
                'created_at': self._fields['created_at'],
            },
            'gitlab': {
                'pipeline_id': self._fields['pipeline_id'],
                'git_sha': self._fields['git_sha'],
                'git_ref': self._fields['git_ref'],
                'git_message': self._fields['git_message'],
                'web_url': self._fields['web_url'],
                'variables': self._fields['variables'],
            }
        }

    def promise_dependencies(self):
        """Return dependencies based on task ids."""
        return []

    def results_data(self):
        """Return the mutable fields for the results."""
        return {
            'gitlab': {
                'status': self._fields['status'],
                'started_at': self._fields['started_at'],
                'finished_at': self._fields['finished_at'],
                'duration': self._fields['duration'],
            }
        }

    def logs_data(self):
        """Return no logs."""
        if False:  # pylint: disable=using-constant-test
            yield

    def artifacts_data(self):
        """Return no artifacts."""
        if False:  # pylint: disable=using-constant-test
            yield


class GitLabJob(GitLabTask):
    """Represents a GitLab job."""

    def __init__(self, gitlab_project, job_id):
        """Create a job."""
        super().__init__({
            'missing': ('manual', 'created'),
            'promise': ('pending', 'running'),
            'results': ('success', 'failed', 'canceled', 'skipped'),
        }, gitlab_project)
        self._fields['job_id'] = job_id

    def task_id(self):
        """Return the task id."""
        return self._task_id(self._fields['created_at'],
                             self._fields['pipeline_id'],
                             self._fields['job_id'],
                             self._fields['task_type'])

    def retrieve_fields(self):
        """Replace general information with updated info from GitLab."""
        g_job = self._gitlab_project.jobs.get(self._fields['job_id'])
        g_stale_pipeline = GitLabJob._get_stale_pipline(self._gitlab_project, g_job.pipeline['id'])
        self._fields.update({
            'pipeline_id': g_job.pipeline['id'],
            'pipeline_created_at': g_stale_pipeline.created_at,
            'task_type': g_job.stage,
            'name': g_job.name,
            'allow_failure': g_job.allow_failure,
            'status': g_job.status,
            'web_url': g_job.web_url,
            'created_at': g_job.created_at,
            'started_at': g_job.started_at,
            'finished_at': g_job.finished_at,
            'duration': g_job.duration,
            'artifacts': g_job.attributes['artifacts'],
        })

    def promise_data(self):
        """Return the immutable fields for the promise."""
        return {
            'data': {
                'task_type': self._fields['task_type'],
                'created_at': self._fields['created_at'],
            },
            'gitlab': {
                'job_id': self._fields['job_id'],
                'pipeline_id': self._fields['pipeline_id'],
                'name': self._fields['name'],
                'allow_failure': self._fields['allow_failure'],
                'web_url': self._fields['web_url'],
            }
        }

    def promise_dependencies(self):
        """Return the dependencies based on task ids."""
        previous_jobs = []
        for task_type, jobs in GitLabJob._get_jobs(self._gitlab_project, self._fields['pipeline_id']).items():
            if task_type == self._fields['task_type']:
                break
            previous_jobs = jobs
        return [{
            'task_type': 'pipeline',
            'task_id': self._task_id(self._fields['pipeline_created_at'], self._fields['pipeline_id'], 'pipeline')
        }] + previous_jobs

    def results_data(self):
        """Return the mutable fields for the results."""
        return {
            'gitlab': {
                'status': self._fields['status'],
                'started_at': self._fields['started_at'],
                'finished_at': self._fields['finished_at'],
                'duration': self._fields['duration'],
            }
        }

    def logs_data(self):
        """Return the log."""
        if not any(a['file_type'] == 'trace' for a in self._fields['artifacts']):
            return
        data = self._gitlab_project.jobs.get(self._fields['job_id'], lazy=True).trace()
        yield 'job.log', io.BytesIO(data), len(data)

    def artifacts_data(self):
        """Return the artifacts."""
        if not any(a['file_type'] == 'archive' for a in self._fields['artifacts']):
            return
        zip_data = self._gitlab_project.jobs.get(self._fields['job_id'], lazy=True).artifacts()
        with zipfile.ZipFile(io.BytesIO(zip_data)) as zip_file:
            for info in zip_file.infolist():
                if not info.is_dir():
                    yield info.filename, zip_file.open(info), info.file_size
