"""Storage task synchronization."""
import typing

from .storage import Storage


class StorageTask:
    """Represents a storage task."""

    _storage: Storage
    _bucket_uri: str
    _storage_uri: str

    def __init__(self, storage: Storage, bucket_uri: str, task_id: str) -> None:
        """Create a task."""
        self._storage = storage
        self._bucket_uri = bucket_uri
        self._storage_uri = self._storage.storage_uri(bucket_uri, task_id)

    def storage_status(self) -> bool:
        """Return the status of the task in storage."""
        return self._storage.task_status(self._storage_uri)

    def create_promise(self, dependencies: typing.List, data: typing.Dict) -> None:
        """Create a promise for the task in storage."""
        dependency_uris = [{
            'task_type': d['task_type'],
            'storage_uri': self._storage.storage_uri(self._bucket_uri, d['task_id'])
        } for d in dependencies]
        self._storage.ensure_bucket(self._bucket_uri)
        self._storage.create_promise(self._storage_uri, dependency_uris, data)

    def create_results(self, task_type: str, data: typing.Dict) -> None:
        """Create results for the task in storage."""
        self._storage.create_results(self._storage_uri, f'{task_type}_results', data)

    def post_artifact(self, path: str, data: typing.BinaryIO, length: int) -> None:
        """Create an artifact of the task in storage."""
        self._storage.post_artifact(self._storage_uri, path, data, length)
