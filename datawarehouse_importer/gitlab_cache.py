"""GitLab pipline and job cache."""
import functools

import gitlab
import requests

from . import gitlab_task


class GitLabCache:
    """Keep a cache for GitLab piplines and jobs."""

    def __init__(self, gitlab_url, gitlab_token):
        """Create a new cache."""
        session = requests.Session()
        session.hooks = {'response': GitLabCache._print_request}
        self._gitlab_api = gitlab.Gitlab(gitlab_url, gitlab_token, session=session)
        self.pipelines = {}

    @staticmethod
    def _print_request(response: requests.Response, *args, **kwargs):  # pylint: disable=unused-argument
        print(f'GitLab API call to {response.url}')

    @functools.lru_cache(maxsize=None)
    def _get_gitlab_project(self, project_id):
        return self._gitlab_api.projects.get(project_id)

    def _update_pipeline(self, project_id, pipeline_id, update_fields=False, update_job=None):
        gitlab_project = self._get_gitlab_project(project_id)
        if project_id not in self.pipelines:
            self.pipelines[project_id] = {}
        if pipeline_id not in self.pipelines[project_id]:
            self.pipelines[project_id][pipeline_id] = gitlab_task.GitLabPipeline(gitlab_project, pipeline_id)
            self.pipelines[project_id][pipeline_id].retrieve_fields()
            self.pipelines[project_id][pipeline_id].retrieve_jobs()
        else:
            if update_fields:
                self.pipelines[project_id][pipeline_id].retrieve_fields()
            if update_job is not None:
                self.pipelines[project_id][pipeline_id].retrieve_job(update_job)
        return self.pipelines[project_id][pipeline_id]

    def get_last_pipeline_ids(self, project_id, count):
        """Get the last count pipeline ids."""
        gitlab_project = self._get_gitlab_project(project_id)
        return reversed([p.id for p in gitlab_project.pipelines.list(page=1, per_page=count)])

    def get_job_ids(self, project_id, pipeline_id):
        """Get the job ids for a certain pipeline."""
        gitlab_project = self._get_gitlab_project(project_id)
        return [j.id for j in gitlab_project.pipelines.get(pipeline_id).jobs.list(as_list=False)]

    def process_pipeline(self, project_id, pipeline_id, event_data=None):
        """Process a status change of a pipeline."""
        pipeline = self._update_pipeline(project_id, pipeline_id, update_fields=True)
        return {
            'project_id': project_id,
            'pipeline_id': pipeline_id,
            'data': pipeline.recursive_data(),
            'event_data': event_data,
            'event_reason': f'P{pipeline_id}',
        }, pipeline

    def process_job(self, project_id, job_id, event_data=None):
        """Process a status change of a job."""
        gitlab_project = self._get_gitlab_project(project_id)
        pipeline_id = gitlab_project.jobs.get(job_id).pipeline['id']
        pipeline, job = self._update_pipeline(project_id, pipeline_id, update_job=job_id)
        return {
            'project_id': project_id,
            'pipeline_id': pipeline_id,
            'data': pipeline.recursive_data(),
            'event_data': event_data,
            'event_reason': f'J{job_id}',
        }, job, pipeline_id

    def pipeline_status(self, project_id, pipeline_id):
        """Return the expected storage status of a pipeline."""
        gitlab_project = self._get_gitlab_project(project_id)
        pipeline = gitlab_task.GitLabPipeline(gitlab_project, pipeline_id)
        pipeline.retrieve_fields()
        return pipeline

    def job_status(self, project_id, job_id):
        """Return the expected storage status of a pipeline."""
        gitlab_project = self._get_gitlab_project(project_id)
        job = gitlab_task.GitLabJob(gitlab_project, job_id)
        job.retrieve_fields()
        return job
