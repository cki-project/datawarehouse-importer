# Development

Define the necessary variables in your environment:

- `AWS_SECRET_ACCESS_KEY`: MinIO secret key
- `AWS_ACCESS_KEY_ID`: MinIO access key
- `MINIO_ENDPOINT`: MinIO endpoint
- `GITLAB_URL`: GitLab instance URL (https:////xci32.lab.eng.rdu2.redhat.com/)
- `XCI32_GITLAB_PROJECT`: GitLab project for the pipelines (2)
- `GITLAB_TOKEN`: GitLab API token
- `WEBHOOK_SECRET_TOKEN`: GitLab webhook secret token
- `SECRET_KEY`: arbitrary secret key for CSRF protection
- `STORAGE_TASK_TYPES`: storage task types to copy (pipeline:lint:merge:build:test)
- `STORAGE_EXCLUDE_ARTIFACTS`: excluded artifacts (build/:merge/)
- `REQUESTS_CA_BUNDLE`: RedHat certificates (/etc/ssl/certs/ca-certificates.crt)

Build the containers:

```shell
buildah bud -f Dockerfile.worker -t datawarehouse-importer-worker .
buildah bud -f Dockerfile.gui -t datawarehouse-importer-gui .
```

Create a pod for the importer and deploy 4 containers:

```shell
podman pod create -n datawarehouse-importer -p 9000 -p 5000
podman pod start datawarehouse-importer

podman create --pod datawarehouse-importer --name datawarehouse-importer-minio -e MINIO_ACCESS_KEY=$AWS_ACCESS_KEY_ID -e MINIO_SECRET_KEY=$AWS_SECRET_ACCESS_KEY docker.io/minio/minio:latest server /data
podman start datawarehouse-importer-minio

podman create --pod datawarehouse-importer --name datawarehouse-importer-redis docker.io/redis:latest
podman start datawarehouse-importer-redis

podman create \
    --pod datawarehouse-importer \
    --name datawarehouse-importer-worker \
    -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
    -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
    -e GITLAB_URL=$GITLAB_URL \
    -e GITLAB_TOKEN=$GITLAB_TOKEN \
    -e SECRET_KEY=$SECRET_KEY \
    -e STORAGE_TASK_TYPES=$STORAGE_TASK_TYPES \
    -e STORAGE_EXCLUDE_ARTIFACTS=$STORAGE_EXCLUDE_ARTIFACTS \
    -v .:/code \
    --ulimit nofile=90000:90000 \
    localhost/datawarehouse-importer-worker:latest
podman start datawarehouse-importer-worker

podman create \
    --pod datawarehouse-importer \
    --name datawarehouse-importer-gui \
    -e WEBHOOK_SECRET_TOKEN=$WEBHOOK_SECRET_TOKEN \
    -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
    -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
    -e SECRET_KEY=$SECRET_KEY \
    --ulimit nofile=90000:90000 \
    -v .:/code \
    localhost/datawarehouse-importer-gui:latest
podman start datawarehouse-importer-gui
```

Install `tox` into the worker container:

```shell
exec podman exec -it --workdir /code datawarehouse-importer-worker pip install tox
```

The source code is bind-mounted directly into the containers.
