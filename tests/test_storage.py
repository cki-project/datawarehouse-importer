"""Tests for the storage interface."""
import io
import os
import unittest

import requests

from datawarehouse_importer import storage


class TestStorage(unittest.TestCase):
    """Test the storage interface."""

    def setUp(self):
        """Prepare the storage test harness."""
        self.endpoint = os.getenv('MINIO_ENDPOINT', 'localhost:9000')
        self.storage = storage.Storage(self.endpoint,
                                       access_key=os.getenv('AWS_ACCESS_KEY_ID'),
                                       secret_key=os.getenv('AWS_SECRET_ACCESS_KEY'))

        self._clear_bucket()

    def tearDown(self):
        """Remove the minio test harness."""
        self._clear_bucket()

    def _clear_bucket(self):
        try:
            for obj in self.storage.client.list_objects('test-bucket', recursive=True):
                self.storage.client.remove_object(obj.bucket_name, obj.object_name)
            self.storage.client.remove_bucket('test-bucket')
        except Exception:  # pylint: disable=broad-except
            pass

    def test_ensure_bucket(self):
        """Test that files in the bucket are accessible."""
        obj_content = b'ABCDEF'
        bucket = 'test-bucket'
        prefix = 'prefix/'

        try:
            self.storage.ensure_bucket(f's3://{bucket}/{prefix}')
            self.storage.ensure_bucket(f's3://{bucket}/{prefix}')
        except Exception:  # pylint: disable=broad-except
            self.fail('Unexpected exception')
        self.assertTrue(self.storage.client.bucket_exists(bucket))

        self.storage.client.put_object(bucket, f'{prefix}test-obj',
                                       io.BytesIO(obj_content), len(obj_content))
        self.assertEqual(requests.get(f'http://{self.endpoint}/{bucket}/{prefix}test-obj').content,
                         obj_content)

        self.storage.client.put_object(bucket, 'test-obj',
                                       io.BytesIO(obj_content), len(obj_content))
        self.assertEqual(requests.get(f'http://{self.endpoint}/{bucket}/test-obj').status_code, 403)
