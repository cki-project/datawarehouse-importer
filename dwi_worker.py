#!/usr/bin/env python3
"""Import data into datawarehouse storage."""
# pylint: disable=invalid-name
import argparse
import copy
import json
import os
import re
import time
import traceback
import typing

from hotqueue import HotQueue
from redis import Redis

from datawarehouse_importer.gitlab_cache import GitLabCache
from datawarehouse_importer.gitlab_task import GitLabTask
from datawarehouse_importer.storage import Storage
from datawarehouse_importer.storage_task import StorageTask


redis = Redis()
gitlab_queue = HotQueue('gitlab_queue', serializer=json)
task_queue = HotQueue('task_queue', serializer=json)


def _is_included(task_type, object_name):
    return not any(re.match(a, f'{task_type}/{object_name}') for a in args.exclude_artifacts)


def _notify_task_event(task_event: typing.Dict) -> None:
    task_id = task_event['task_id']
    redis.hset('task_cache', task_id, json.dumps(task_event))
    redis.zadd('task_cache_index', {task_id: redis.incr('task_cache_last_index')})
    task_cache_count = redis.zcard('task_cache_index')
    max_task_cache_count = 10000
    if task_cache_count > max_task_cache_count:
        task_ids = redis.zrange('task_cache_index', 0, task_cache_count - max_task_cache_count - 1)
        redis.zrem('task_cache_index', *task_ids)
        redis.hdel('task_cache', *task_ids)
    task_queue.put(task_id)


def _process_task_promise(task: GitLabTask, storage_task: StorageTask) -> None:
    storage_task.create_promise(task.promise_dependencies(), task.promise_data())


def _process_task_results(task: GitLabTask, storage_task: StorageTask) -> None:
    for log_data in task.logs_data():
        storage_task.post_artifact(*log_data)
    if _is_included(task.task_type(), '__NON_EXISTING_NAME'):
        for object_name, artifact, artifact_length in task.artifacts_data():
            if _is_included(task.task_type(), object_name):
                storage_task.post_artifact(object_name, artifact, artifact_length)
    storage_task.create_results(f'{task.task_type()}_results', task.results_data())


def _process_task_event(task: GitLabTask) -> None:
    task_id = task.task_id()
    task_event = copy.deepcopy(task.data())
    task_event['task_id'] = task_id

    try:
        expected_storage_status = task.expected_storage_status()
        if (len(args.task_types) > 0) and (task.task_type() not in args.task_types):
            expected_storage_status = 'missing'
        storage_task = StorageTask(storage, args.bucket_uri, task_id)
        storage_status, storage_size = storage_task.storage_status()
        task_event['expected_storage_status'] = expected_storage_status
        task_event['storage_status'] = storage_status
        task_event['storage_size'] = storage_size

        # TODO hack to get a browsable link
        endpoint = re.sub('^.*://', '', args.endpoint)
        endpoint = re.sub('/.*', '', endpoint)
        endpoint = 'http://' + endpoint + '/minio/'
        task_event['storage_url'] = re.sub('[^/]*$', '', re.sub(
            '^s3://', endpoint, storage.storage_uri(args.bucket_uri, task_id)))

        if expected_storage_status != storage_status:
            if expected_storage_status in ('promise', 'results') and storage_status == 'missing':
                task_event['in_progress'] = True
                _notify_task_event(task_event)
                _process_task_promise(task, storage_task)
                storage_status, storage_size = storage_task.storage_status()
                task_event['storage_status'] = storage_status
                task_event['storage_size'] = storage_size
            if expected_storage_status == 'results' and storage_status == 'promise':
                task_event['in_progress'] = True
                _notify_task_event(task_event)
                _process_task_results(task, storage_task)
                storage_status, storage_size = storage_task.storage_status()
                task_event['storage_status'] = storage_status
                task_event['storage_size'] = storage_size
    except Exception:
        task_event['error'] = traceback.format_exc()
        raise
    finally:
        task_event['in_progress'] = False
        _notify_task_event(task_event)


class Worker:
    """Worker methods that can be called via the queue."""

    @staticmethod
    def process_pipeline(project_id, pipeline_id):
        """Process a status change of a pipeline."""
        _process_task_event(gitlab_cache.pipeline_status(project_id, pipeline_id))

    @staticmethod
    def process_pipelines(project_id, count):
        """Enqueue the last count pipelines to check for changes."""
        for pipeline_id in gitlab_cache.get_last_pipeline_ids(project_id, count):
            gitlab_queue_put('process_pipeline', project_id, pipeline_id)
            gitlab_queue_put('process_jobs', project_id, pipeline_id)

    @staticmethod
    def process_jobs(project_id, pipeline_id):
        """Enqueue all jobs of a pipeline to check for changes."""
        for job_id in gitlab_cache.get_job_ids(project_id, pipeline_id):
            gitlab_queue_put('process_job', project_id, job_id)

    @staticmethod
    def process_job(project_id, job_id):
        """Process a status change of a job."""
        _process_task_event(gitlab_cache.job_status(project_id, job_id))


def colon_set(string: str) -> typing.Set[str]:
    """Convert a colon-separated string into a set."""
    return set(string.split(':'))


def gitlab_queue_put(func, *aargs, **kwargs):
    """Enqueue a worker function to serialize GitLab access."""
    gitlab_queue.put((func, aargs, kwargs))


@gitlab_queue.worker
def gitlab_worker(item=None):
    """Serialize GitLab access."""
    while redis.get('gitlab_queue_disabled'):
        time.sleep(1)
    try:
        func, aargs, kwargs = item
        if func.startswith('process_'):
            getattr(Worker, func)(*aargs, **kwargs)
    except Exception:  # pylint: disable=broad-except
        traceback.print_exc()
        # TODO: notify sentry, retry


parser = argparse.ArgumentParser(description='Import data into datawarehouse storage.')
parser.add_argument('--endpoint', default=os.getenv('MINIO_ENDPOINT', 'localhost:9000'), help='S3 endpoint')
parser.add_argument('--access-key', default=os.getenv('AWS_ACCESS_KEY_ID'), help='S3 access key')
parser.add_argument('--secret-key', default=os.getenv('AWS_SECRET_ACCESS_KEY'), help='S3 secret key')
parser.add_argument('--gitlab-token', default=os.getenv('GITLAB_TOKEN'), help='Gitlab secret token')
parser.add_argument('--gitlab-url', default=os.getenv('GITLAB_URL'), help='Gitlab URL')
parser.add_argument('--task-types', default=os.getenv('STORAGE_TASK_TYPES', ''), type=colon_set,
                    help='Colon-separated list of task types to import into storage')
parser.add_argument('--exclude-artifacts', default=os.getenv('STORAGE_EXCLUDE_ARTIFACTS', ''), type=colon_set,
                    help='Colon-separated list of task-type/reg-exps to exclude artifacts based on paths')
parser.add_argument('bucket_uri', help='S3 bucket URI in s3://bucket format')
args = parser.parse_args()


gitlab_cache = GitLabCache(args.gitlab_url, args.gitlab_token)
storage = Storage(args.endpoint, args.access_key, args.secret_key)

gitlab_worker()
